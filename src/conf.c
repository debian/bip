/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "conf.y"

/*
 * $Id: conf.y,v 1.26 2005/04/17 15:20:32 nohar Exp $
 *
 * This file is part of the bip proproject
 * Copyright (C) 2004 Arnaud Cornet
 * Copyright (C) 2022 Loïc Gomez
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * See the file "COPYING" for the exact licensing terms.
 */

#include <stdlib.h>
#include "util.h"
#include "irc.h"
#include "tuple.h"
extern int yylex (void);
extern char *yytext;
extern int linec;

#define YYMALLOC bip_malloc

extern int yyerror(char *);

int yywrap(void)
{
	return 1;
}

//int yydebug = 1;

list_t *root_list;

struct tuple *tuple_i_new(int type, int i)
{
	struct tuple *t;
	t = bip_malloc(sizeof(struct tuple));
	t->type = type;
	t->ndata = i;
	t->tuple_type = TUPLE_INT;
	return t;
}

struct tuple *tuple_p_new(int type, void *p)
{
	struct tuple *t;
	t = bip_malloc(sizeof(struct tuple));
	t->type = type;
	t->pdata = p;
	return t;
}

struct tuple *tuple_s_new(int type, void *p)
{
	struct tuple *t = tuple_p_new(type, p);
	t->tuple_type = TUPLE_STR;
	return t;
}

struct tuple *tuple_l_new(int type, void *p)
{
	struct tuple *t = tuple_p_new(type, p);
	t->tuple_type = TUPLE_LIST;
	return t;
}


#line 142 "conf.c"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_CONF_H_INCLUDED
# define YY_YY_CONF_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    LEX_IP = 258,                  /* LEX_IP  */
    LEX_EQ = 259,                  /* LEX_EQ  */
    LEX_PORT = 260,                /* LEX_PORT  */
    LEX_CSS = 261,                 /* LEX_CSS  */
    LEX_SEMICOLON = 262,           /* LEX_SEMICOLON  */
    LEX_CONNECTION = 263,          /* LEX_CONNECTION  */
    LEX_NETWORK = 264,             /* LEX_NETWORK  */
    LEX_LBRA = 265,                /* LEX_LBRA  */
    LEX_RBRA = 266,                /* LEX_RBRA  */
    LEX_USER = 267,                /* LEX_USER  */
    LEX_NAME = 268,                /* LEX_NAME  */
    LEX_NICK = 269,                /* LEX_NICK  */
    LEX_SERVER = 270,              /* LEX_SERVER  */
    LEX_PASSWORD = 271,            /* LEX_PASSWORD  */
    LEX_SRCIP = 272,               /* LEX_SRCIP  */
    LEX_HOST = 273,                /* LEX_HOST  */
    LEX_VHOST = 274,               /* LEX_VHOST  */
    LEX_SOURCE_PORT = 275,         /* LEX_SOURCE_PORT  */
    LEX_NONE = 276,                /* LEX_NONE  */
    LEX_COMMENT = 277,             /* LEX_COMMENT  */
    LEX_BUNCH = 278,               /* LEX_BUNCH  */
    LEX_REALNAME = 279,            /* LEX_REALNAME  */
    LEX_SSL = 280,                 /* LEX_SSL  */
    LEX_SSL_CHECK_MODE = 281,      /* LEX_SSL_CHECK_MODE  */
    LEX_SSL_CHECK_STORE = 282,     /* LEX_SSL_CHECK_STORE  */
    LEX_SSL_CLIENT_CERTFILE = 283, /* LEX_SSL_CLIENT_CERTFILE  */
    LEX_CIPHERS = 284,             /* LEX_CIPHERS  */
    LEX_CSS_CIPHERS = 285,         /* LEX_CSS_CIPHERS  */
    LEX_DEFAULT_CIPHERS = 286,     /* LEX_DEFAULT_CIPHERS  */
    LEX_DH_PARAM = 287,            /* LEX_DH_PARAM  */
    LEX_CHANNEL = 288,             /* LEX_CHANNEL  */
    LEX_KEY = 289,                 /* LEX_KEY  */
    LEX_LOG_ROOT = 290,            /* LEX_LOG_ROOT  */
    LEX_LOG_FORMAT = 291,          /* LEX_LOG_FORMAT  */
    LEX_LOG_LEVEL = 292,           /* LEX_LOG_LEVEL  */
    LEX_BACKLOG_LINES = 293,       /* LEX_BACKLOG_LINES  */
    LEX_BACKLOG_TIMESTAMP = 294,   /* LEX_BACKLOG_TIMESTAMP  */
    LEX_BACKLOG_NO_TIMESTAMP = 295, /* LEX_BACKLOG_NO_TIMESTAMP  */
    LEX_BACKLOG = 296,             /* LEX_BACKLOG  */
    LEX_LOG = 297,                 /* LEX_LOG  */
    LEX_LOG_SYSTEM = 298,          /* LEX_LOG_SYSTEM  */
    LEX_LOG_SYNC_INTERVAL = 299,   /* LEX_LOG_SYNC_INTERVAL  */
    LEX_FOLLOW_NICK = 300,         /* LEX_FOLLOW_NICK  */
    LEX_ON_CONNECT_SEND = 301,     /* LEX_ON_CONNECT_SEND  */
    LEX_AWAY_NICK = 302,           /* LEX_AWAY_NICK  */
    LEX_PID_FILE = 303,            /* LEX_PID_FILE  */
    LEX_WRITE_OIDENTD = 304,       /* LEX_WRITE_OIDENTD  */
    LEX_OIDENTD_FILE = 305,        /* LEX_OIDENTD_FILE  */
    LEX_IGN_FIRST_NICK = 306,      /* LEX_IGN_FIRST_NICK  */
    LEX_ALWAYS_BACKLOG = 307,      /* LEX_ALWAYS_BACKLOG  */
    LEX_BLRESET_ON_TALK = 308,     /* LEX_BLRESET_ON_TALK  */
    LEX_BLRESET_CONNECTION = 309,  /* LEX_BLRESET_CONNECTION  */
    LEX_DEFAULT_USER = 310,        /* LEX_DEFAULT_USER  */
    LEX_DEFAULT_NICK = 311,        /* LEX_DEFAULT_NICK  */
    LEX_DEFAULT_REALNAME = 312,    /* LEX_DEFAULT_REALNAME  */
    LEX_NO_CLIENT_AWAY_MSG = 313,  /* LEX_NO_CLIENT_AWAY_MSG  */
    LEX_BL_MSG_ONLY = 314,         /* LEX_BL_MSG_ONLY  */
    LEX_ADMIN = 315,               /* LEX_ADMIN  */
    LEX_BIP_USE_NOTICE = 316,      /* LEX_BIP_USE_NOTICE  */
    LEX_CSS_PEM = 317,             /* LEX_CSS_PEM  */
    LEX_AUTOJOIN_ON_KICK = 318,    /* LEX_AUTOJOIN_ON_KICK  */
    LEX_IGNORE_CAPAB = 319,        /* LEX_IGNORE_CAPAB  */
    LEX_RECONN_TIMER = 320,        /* LEX_RECONN_TIMER  */
    LEX_SASL_USERNAME = 321,       /* LEX_SASL_USERNAME  */
    LEX_SASL_PASSWORD = 322,       /* LEX_SASL_PASSWORD  */
    LEX_SASL_MECHANISM = 323,      /* LEX_SASL_MECHANISM  */
    LEX_BOOL = 324,                /* LEX_BOOL  */
    LEX_INT = 325,                 /* LEX_INT  */
    LEX_STRING = 326               /* LEX_STRING  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define LEX_IP 258
#define LEX_EQ 259
#define LEX_PORT 260
#define LEX_CSS 261
#define LEX_SEMICOLON 262
#define LEX_CONNECTION 263
#define LEX_NETWORK 264
#define LEX_LBRA 265
#define LEX_RBRA 266
#define LEX_USER 267
#define LEX_NAME 268
#define LEX_NICK 269
#define LEX_SERVER 270
#define LEX_PASSWORD 271
#define LEX_SRCIP 272
#define LEX_HOST 273
#define LEX_VHOST 274
#define LEX_SOURCE_PORT 275
#define LEX_NONE 276
#define LEX_COMMENT 277
#define LEX_BUNCH 278
#define LEX_REALNAME 279
#define LEX_SSL 280
#define LEX_SSL_CHECK_MODE 281
#define LEX_SSL_CHECK_STORE 282
#define LEX_SSL_CLIENT_CERTFILE 283
#define LEX_CIPHERS 284
#define LEX_CSS_CIPHERS 285
#define LEX_DEFAULT_CIPHERS 286
#define LEX_DH_PARAM 287
#define LEX_CHANNEL 288
#define LEX_KEY 289
#define LEX_LOG_ROOT 290
#define LEX_LOG_FORMAT 291
#define LEX_LOG_LEVEL 292
#define LEX_BACKLOG_LINES 293
#define LEX_BACKLOG_TIMESTAMP 294
#define LEX_BACKLOG_NO_TIMESTAMP 295
#define LEX_BACKLOG 296
#define LEX_LOG 297
#define LEX_LOG_SYSTEM 298
#define LEX_LOG_SYNC_INTERVAL 299
#define LEX_FOLLOW_NICK 300
#define LEX_ON_CONNECT_SEND 301
#define LEX_AWAY_NICK 302
#define LEX_PID_FILE 303
#define LEX_WRITE_OIDENTD 304
#define LEX_OIDENTD_FILE 305
#define LEX_IGN_FIRST_NICK 306
#define LEX_ALWAYS_BACKLOG 307
#define LEX_BLRESET_ON_TALK 308
#define LEX_BLRESET_CONNECTION 309
#define LEX_DEFAULT_USER 310
#define LEX_DEFAULT_NICK 311
#define LEX_DEFAULT_REALNAME 312
#define LEX_NO_CLIENT_AWAY_MSG 313
#define LEX_BL_MSG_ONLY 314
#define LEX_ADMIN 315
#define LEX_BIP_USE_NOTICE 316
#define LEX_CSS_PEM 317
#define LEX_AUTOJOIN_ON_KICK 318
#define LEX_IGNORE_CAPAB 319
#define LEX_RECONN_TIMER 320
#define LEX_SASL_USERNAME 321
#define LEX_SASL_PASSWORD 322
#define LEX_SASL_MECHANISM 323
#define LEX_BOOL 324
#define LEX_INT 325
#define LEX_STRING 326

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 74 "conf.y"

	int number;
	char *string;
	void *list;
	struct tuple *tuple;

#line 344 "conf.c"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_CONF_H_INCLUDED  */
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_LEX_IP = 3,                     /* LEX_IP  */
  YYSYMBOL_LEX_EQ = 4,                     /* LEX_EQ  */
  YYSYMBOL_LEX_PORT = 5,                   /* LEX_PORT  */
  YYSYMBOL_LEX_CSS = 6,                    /* LEX_CSS  */
  YYSYMBOL_LEX_SEMICOLON = 7,              /* LEX_SEMICOLON  */
  YYSYMBOL_LEX_CONNECTION = 8,             /* LEX_CONNECTION  */
  YYSYMBOL_LEX_NETWORK = 9,                /* LEX_NETWORK  */
  YYSYMBOL_LEX_LBRA = 10,                  /* LEX_LBRA  */
  YYSYMBOL_LEX_RBRA = 11,                  /* LEX_RBRA  */
  YYSYMBOL_LEX_USER = 12,                  /* LEX_USER  */
  YYSYMBOL_LEX_NAME = 13,                  /* LEX_NAME  */
  YYSYMBOL_LEX_NICK = 14,                  /* LEX_NICK  */
  YYSYMBOL_LEX_SERVER = 15,                /* LEX_SERVER  */
  YYSYMBOL_LEX_PASSWORD = 16,              /* LEX_PASSWORD  */
  YYSYMBOL_LEX_SRCIP = 17,                 /* LEX_SRCIP  */
  YYSYMBOL_LEX_HOST = 18,                  /* LEX_HOST  */
  YYSYMBOL_LEX_VHOST = 19,                 /* LEX_VHOST  */
  YYSYMBOL_LEX_SOURCE_PORT = 20,           /* LEX_SOURCE_PORT  */
  YYSYMBOL_LEX_NONE = 21,                  /* LEX_NONE  */
  YYSYMBOL_LEX_COMMENT = 22,               /* LEX_COMMENT  */
  YYSYMBOL_LEX_BUNCH = 23,                 /* LEX_BUNCH  */
  YYSYMBOL_LEX_REALNAME = 24,              /* LEX_REALNAME  */
  YYSYMBOL_LEX_SSL = 25,                   /* LEX_SSL  */
  YYSYMBOL_LEX_SSL_CHECK_MODE = 26,        /* LEX_SSL_CHECK_MODE  */
  YYSYMBOL_LEX_SSL_CHECK_STORE = 27,       /* LEX_SSL_CHECK_STORE  */
  YYSYMBOL_LEX_SSL_CLIENT_CERTFILE = 28,   /* LEX_SSL_CLIENT_CERTFILE  */
  YYSYMBOL_LEX_CIPHERS = 29,               /* LEX_CIPHERS  */
  YYSYMBOL_LEX_CSS_CIPHERS = 30,           /* LEX_CSS_CIPHERS  */
  YYSYMBOL_LEX_DEFAULT_CIPHERS = 31,       /* LEX_DEFAULT_CIPHERS  */
  YYSYMBOL_LEX_DH_PARAM = 32,              /* LEX_DH_PARAM  */
  YYSYMBOL_LEX_CHANNEL = 33,               /* LEX_CHANNEL  */
  YYSYMBOL_LEX_KEY = 34,                   /* LEX_KEY  */
  YYSYMBOL_LEX_LOG_ROOT = 35,              /* LEX_LOG_ROOT  */
  YYSYMBOL_LEX_LOG_FORMAT = 36,            /* LEX_LOG_FORMAT  */
  YYSYMBOL_LEX_LOG_LEVEL = 37,             /* LEX_LOG_LEVEL  */
  YYSYMBOL_LEX_BACKLOG_LINES = 38,         /* LEX_BACKLOG_LINES  */
  YYSYMBOL_LEX_BACKLOG_TIMESTAMP = 39,     /* LEX_BACKLOG_TIMESTAMP  */
  YYSYMBOL_LEX_BACKLOG_NO_TIMESTAMP = 40,  /* LEX_BACKLOG_NO_TIMESTAMP  */
  YYSYMBOL_LEX_BACKLOG = 41,               /* LEX_BACKLOG  */
  YYSYMBOL_LEX_LOG = 42,                   /* LEX_LOG  */
  YYSYMBOL_LEX_LOG_SYSTEM = 43,            /* LEX_LOG_SYSTEM  */
  YYSYMBOL_LEX_LOG_SYNC_INTERVAL = 44,     /* LEX_LOG_SYNC_INTERVAL  */
  YYSYMBOL_LEX_FOLLOW_NICK = 45,           /* LEX_FOLLOW_NICK  */
  YYSYMBOL_LEX_ON_CONNECT_SEND = 46,       /* LEX_ON_CONNECT_SEND  */
  YYSYMBOL_LEX_AWAY_NICK = 47,             /* LEX_AWAY_NICK  */
  YYSYMBOL_LEX_PID_FILE = 48,              /* LEX_PID_FILE  */
  YYSYMBOL_LEX_WRITE_OIDENTD = 49,         /* LEX_WRITE_OIDENTD  */
  YYSYMBOL_LEX_OIDENTD_FILE = 50,          /* LEX_OIDENTD_FILE  */
  YYSYMBOL_LEX_IGN_FIRST_NICK = 51,        /* LEX_IGN_FIRST_NICK  */
  YYSYMBOL_LEX_ALWAYS_BACKLOG = 52,        /* LEX_ALWAYS_BACKLOG  */
  YYSYMBOL_LEX_BLRESET_ON_TALK = 53,       /* LEX_BLRESET_ON_TALK  */
  YYSYMBOL_LEX_BLRESET_CONNECTION = 54,    /* LEX_BLRESET_CONNECTION  */
  YYSYMBOL_LEX_DEFAULT_USER = 55,          /* LEX_DEFAULT_USER  */
  YYSYMBOL_LEX_DEFAULT_NICK = 56,          /* LEX_DEFAULT_NICK  */
  YYSYMBOL_LEX_DEFAULT_REALNAME = 57,      /* LEX_DEFAULT_REALNAME  */
  YYSYMBOL_LEX_NO_CLIENT_AWAY_MSG = 58,    /* LEX_NO_CLIENT_AWAY_MSG  */
  YYSYMBOL_LEX_BL_MSG_ONLY = 59,           /* LEX_BL_MSG_ONLY  */
  YYSYMBOL_LEX_ADMIN = 60,                 /* LEX_ADMIN  */
  YYSYMBOL_LEX_BIP_USE_NOTICE = 61,        /* LEX_BIP_USE_NOTICE  */
  YYSYMBOL_LEX_CSS_PEM = 62,               /* LEX_CSS_PEM  */
  YYSYMBOL_LEX_AUTOJOIN_ON_KICK = 63,      /* LEX_AUTOJOIN_ON_KICK  */
  YYSYMBOL_LEX_IGNORE_CAPAB = 64,          /* LEX_IGNORE_CAPAB  */
  YYSYMBOL_LEX_RECONN_TIMER = 65,          /* LEX_RECONN_TIMER  */
  YYSYMBOL_LEX_SASL_USERNAME = 66,         /* LEX_SASL_USERNAME  */
  YYSYMBOL_LEX_SASL_PASSWORD = 67,         /* LEX_SASL_PASSWORD  */
  YYSYMBOL_LEX_SASL_MECHANISM = 68,        /* LEX_SASL_MECHANISM  */
  YYSYMBOL_LEX_BOOL = 69,                  /* LEX_BOOL  */
  YYSYMBOL_LEX_INT = 70,                   /* LEX_INT  */
  YYSYMBOL_LEX_STRING = 71,                /* LEX_STRING  */
  YYSYMBOL_YYACCEPT = 72,                  /* $accept  */
  YYSYMBOL_commands = 73,                  /* commands  */
  YYSYMBOL_command = 74,                   /* command  */
  YYSYMBOL_network = 75,                   /* network  */
  YYSYMBOL_net_command = 76,               /* net_command  */
  YYSYMBOL_user = 77,                      /* user  */
  YYSYMBOL_usr_command = 78,               /* usr_command  */
  YYSYMBOL_connection = 79,                /* connection  */
  YYSYMBOL_con_command = 80,               /* con_command  */
  YYSYMBOL_channel = 81,                   /* channel  */
  YYSYMBOL_cha_command = 82,               /* cha_command  */
  YYSYMBOL_server = 83,                    /* server  */
  YYSYMBOL_ser_command = 84                /* ser_command  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   239

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  72
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  13
/* YYNRULES -- Number of rules.  */
#define YYNRULES  88
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  245

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   326


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    89,    89,    90,    94,    95,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   110,   111,
     112,   114,   117,   120,   123,   124,   127,   130,   133,   135,
     138,   139,   142,   143,   144,   145,   149,   150,   153,   155,
     157,   158,   160,   162,   164,   166,   168,   170,   172,   175,
     178,   181,   182,   185,   188,   191,   193,   197,   198,   202,
     203,   205,   206,   207,   208,   210,   212,   214,   216,   218,
     219,   221,   223,   225,   227,   229,   231,   233,   235,   237,
     240,   241,   244,   245,   246,   249,   250,   253,   254
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "LEX_IP", "LEX_EQ",
  "LEX_PORT", "LEX_CSS", "LEX_SEMICOLON", "LEX_CONNECTION", "LEX_NETWORK",
  "LEX_LBRA", "LEX_RBRA", "LEX_USER", "LEX_NAME", "LEX_NICK", "LEX_SERVER",
  "LEX_PASSWORD", "LEX_SRCIP", "LEX_HOST", "LEX_VHOST", "LEX_SOURCE_PORT",
  "LEX_NONE", "LEX_COMMENT", "LEX_BUNCH", "LEX_REALNAME", "LEX_SSL",
  "LEX_SSL_CHECK_MODE", "LEX_SSL_CHECK_STORE", "LEX_SSL_CLIENT_CERTFILE",
  "LEX_CIPHERS", "LEX_CSS_CIPHERS", "LEX_DEFAULT_CIPHERS", "LEX_DH_PARAM",
  "LEX_CHANNEL", "LEX_KEY", "LEX_LOG_ROOT", "LEX_LOG_FORMAT",
  "LEX_LOG_LEVEL", "LEX_BACKLOG_LINES", "LEX_BACKLOG_TIMESTAMP",
  "LEX_BACKLOG_NO_TIMESTAMP", "LEX_BACKLOG", "LEX_LOG", "LEX_LOG_SYSTEM",
  "LEX_LOG_SYNC_INTERVAL", "LEX_FOLLOW_NICK", "LEX_ON_CONNECT_SEND",
  "LEX_AWAY_NICK", "LEX_PID_FILE", "LEX_WRITE_OIDENTD", "LEX_OIDENTD_FILE",
  "LEX_IGN_FIRST_NICK", "LEX_ALWAYS_BACKLOG", "LEX_BLRESET_ON_TALK",
  "LEX_BLRESET_CONNECTION", "LEX_DEFAULT_USER", "LEX_DEFAULT_NICK",
  "LEX_DEFAULT_REALNAME", "LEX_NO_CLIENT_AWAY_MSG", "LEX_BL_MSG_ONLY",
  "LEX_ADMIN", "LEX_BIP_USE_NOTICE", "LEX_CSS_PEM", "LEX_AUTOJOIN_ON_KICK",
  "LEX_IGNORE_CAPAB", "LEX_RECONN_TIMER", "LEX_SASL_USERNAME",
  "LEX_SASL_PASSWORD", "LEX_SASL_MECHANISM", "LEX_BOOL", "LEX_INT",
  "LEX_STRING", "$accept", "commands", "command", "network", "net_command",
  "user", "usr_command", "connection", "con_command", "channel",
  "cha_command", "server", "ser_command", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-7)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      -7,     0,    -7,    -2,     6,     7,    -6,     3,    12,    14,
      16,    17,    18,    19,    21,    23,    24,    30,    41,    42,
      47,    51,    52,    53,    54,    56,    57,    59,    66,     1,
       2,     8,    11,    -7,    -7,     5,    15,    20,    22,    25,
      28,    29,    32,    26,    35,    36,    39,    44,    38,    48,
      40,    60,    61,    62,    45,    50,    -7,    -7,    -7,    -7,
       4,    99,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,
      -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,
      -7,    -7,    -7,    -7,    68,    67,    78,    79,    77,    75,
      -7,    83,    85,    86,    88,    90,   120,   124,   128,   129,
     130,   131,   132,   137,   138,   139,   140,   141,   142,   143,
      76,    -7,    80,    91,    -7,    -7,    92,    93,    94,    95,
      96,    87,    97,   100,   101,   102,   103,   104,   105,   106,
     107,   110,   111,   112,    -7,    -7,    -4,    -7,    -7,    55,
      -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,
      -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,   144,    -7,
     157,   167,   171,    -7,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   193,   194,
     195,   196,   197,   198,   199,   133,   119,    -7,   134,   136,
     145,   146,   147,   148,   150,   151,   152,    -7,   135,   155,
     154,   156,   159,   158,   161,   162,   163,   164,   165,    -7,
      -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,
      -7,    13,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,
      -7,    -7,    -7,    -7,   204,   205,   206,   207,   166,   168,
     169,    -7,    -7,    -7,    -7
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       2,     0,     1,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    30,    36,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     3,     8,     9,    10,
       0,     0,    12,    13,    14,     4,     5,     6,    21,    22,
      23,    24,    15,    16,    17,    18,    19,    20,    27,    25,
      26,    11,     7,    28,     0,     0,     0,     0,     0,     0,
      29,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    85,     0,     0,    31,    57,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    37,    32,     0,    33,    34,     0,
      38,    39,    42,    43,    44,    48,    49,    50,    51,    55,
      52,    53,    45,    46,    47,    54,    40,    41,     0,    35,
       0,     0,     0,    56,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    86,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    80,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    58,
      88,    87,    60,    63,    59,    62,    65,    69,    70,    64,
      79,     0,    61,    72,    77,    71,    73,    78,    74,    75,
      66,    67,    68,    76,     0,     0,     0,     0,     0,     0,
       0,    81,    82,    83,    84
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
      -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,    -7,
      -7,    -7,    -7
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,     1,    29,    60,    88,    61,   109,   139,   184,   221,
     237,   136,   161
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
       2,   158,    30,     3,    33,     4,     5,   159,    56,     6,
      31,    32,     7,    34,   160,    83,    35,    84,    36,    85,
      37,    38,    39,    40,   233,    41,   234,    42,    43,    86,
       8,     9,    10,    87,    44,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    45,    46,   235,    21,    22,
      23,    47,    24,    25,   236,    48,    49,    50,    51,    26,
      52,    53,    27,    54,   162,    28,   163,   164,   165,   166,
      55,   167,   110,    57,   168,   169,    62,   111,    58,   170,
      59,   171,   112,   113,   114,   115,    63,   116,   172,   117,
     118,    64,   119,    65,   120,    70,    66,   173,    67,    68,
     174,   175,   176,    69,    71,    72,   177,    89,    73,    75,
      90,    77,    91,   178,    74,    92,    81,    76,   179,   180,
      82,   181,   182,   183,   121,    93,    94,    95,   122,    78,
      79,    80,   123,   124,   125,   126,   127,    96,    97,    98,
      99,   128,   129,   130,   131,   132,   133,   135,   185,   137,
     134,   100,   101,   102,   103,   104,   105,   145,   106,   107,
     108,   186,   138,   140,   141,   142,   143,   144,   146,   147,
     148,   149,   150,   151,   187,   188,   152,   153,   154,   155,
     156,   157,   189,   190,   191,   192,   193,   194,   195,   196,
     211,   198,   199,   200,   201,   202,   197,   203,   204,   205,
     206,   207,   208,   210,   222,   212,   209,   213,   238,   239,
     240,     0,     0,     0,   241,     0,   214,   215,   216,   217,
     218,     0,   219,   220,   223,   224,     0,   225,   226,   227,
     228,   229,     0,     0,   230,   231,   232,   242,   244,   243
};

static const yytype_int8 yycheck[] =
{
       0,     5,     4,     3,    10,     5,     6,    11,     7,     9,
       4,     4,    12,    10,    18,    11,     4,    13,     4,    15,
       4,     4,     4,     4,    11,     4,    13,     4,     4,    25,
      30,    31,    32,    29,     4,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,     4,     4,    34,    48,    49,
      50,     4,    52,    53,    41,     4,     4,     4,     4,    59,
       4,     4,    62,     4,     9,    65,    11,    12,    13,    14,
       4,    16,     4,    71,    19,    20,    71,    10,    70,    24,
      69,    26,     4,     4,     7,    10,    71,     4,    33,     4,
       4,    71,     4,    71,     4,    69,    71,    42,    70,    70,
      45,    46,    47,    71,    69,    69,    51,     8,    69,    71,
      11,    71,    13,    58,    70,    16,    71,    69,    63,    64,
      70,    66,    67,    68,     4,    26,    27,    28,     4,    69,
      69,    69,     4,     4,     4,     4,     4,    38,    39,    40,
      41,     4,     4,     4,     4,     4,     4,    71,     4,    69,
       7,    52,    53,    54,    55,    56,    57,    70,    59,    60,
      61,     4,    71,    71,    71,    71,    71,    71,    71,    69,
      69,    69,    69,    69,     7,     4,    71,    71,    71,    69,
      69,    69,     4,     4,     4,     4,     4,     4,     4,     4,
      71,     4,     4,     4,     4,     4,    10,     4,     4,     4,
       4,     4,     4,    70,    69,    71,     7,    71,     4,     4,
       4,    -1,    -1,    -1,     7,    -1,    71,    71,    71,    71,
      70,    -1,    71,    71,    69,    71,    -1,    71,    69,    71,
      69,    69,    -1,    -1,    71,    71,    71,    71,    69,    71
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    73,     0,     3,     5,     6,     9,    12,    30,    31,
      32,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    48,    49,    50,    52,    53,    59,    62,    65,    74,
       4,     4,     4,    10,    10,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     7,    71,    70,    69,
      75,    77,    71,    71,    71,    71,    71,    70,    70,    71,
      69,    69,    69,    69,    70,    71,    69,    71,    69,    69,
      69,    71,    70,    11,    13,    15,    25,    29,    76,     8,
      11,    13,    16,    26,    27,    28,    38,    39,    40,    41,
      52,    53,    54,    55,    56,    57,    59,    60,    61,    78,
       4,    10,     4,     4,     7,    10,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     7,    71,    83,    69,    71,    79,
      71,    71,    71,    71,    71,    70,    71,    69,    69,    69,
      69,    69,    71,    71,    71,    69,    69,    69,     5,    11,
      18,    84,     9,    11,    12,    13,    14,    16,    19,    20,
      24,    26,    33,    42,    45,    46,    47,    51,    58,    63,
      64,    66,    67,    68,    80,     4,     4,     7,     4,     4,
       4,     4,     4,     4,     4,     4,     4,    10,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     7,
      70,    71,    71,    71,    71,    71,    71,    71,    70,    71,
      71,    81,    69,    69,    71,    71,    69,    71,    69,    69,
      71,    71,    71,    11,    13,    34,    41,    82,     4,     4,
       4,     7,    71,    71,    69
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    72,    73,    73,    74,    74,    74,    74,    74,    74,
      74,    74,    74,    74,    74,    74,    74,    74,    74,    74,
      74,    74,    74,    74,    74,    74,    74,    74,    74,    74,
      75,    75,    76,    76,    76,    76,    77,    77,    78,    78,
      78,    78,    78,    78,    78,    78,    78,    78,    78,    78,
      78,    78,    78,    78,    78,    78,    78,    79,    79,    80,
      80,    80,    80,    80,    80,    80,    80,    80,    80,    80,
      80,    80,    80,    80,    80,    80,    80,    80,    80,    80,
      81,    81,    82,    82,    82,    83,    83,    84,    84
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     0,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     4,     4,
       0,     3,     3,     3,     3,     4,     0,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     4,     0,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     4,     3,     3,     3,
       0,     3,     3,     3,     3,     0,     3,     3,     3
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* commands: %empty  */
#line 89 "conf.y"
        { (yyval.list) = root_list = list_new(NULL); }
#line 1563 "conf.c"
    break;

  case 3: /* commands: commands command LEX_SEMICOLON  */
#line 90 "conf.y"
                                         { list_add_last((yyvsp[-2].list), (yyvsp[-1].tuple)); (yyval.list) = (yyvsp[-2].list); }
#line 1569 "conf.c"
    break;

  case 4: /* command: LEX_LOG_ROOT LEX_EQ LEX_STRING  */
#line 94 "conf.y"
                                      { (yyval.tuple) = tuple_s_new(LEX_LOG_ROOT, (yyvsp[0].string)); }
#line 1575 "conf.c"
    break;

  case 5: /* command: LEX_LOG_FORMAT LEX_EQ LEX_STRING  */
#line 95 "conf.y"
                                          { (yyval.tuple) = tuple_s_new(LEX_LOG_FORMAT,
						(yyvsp[0].string)); }
#line 1582 "conf.c"
    break;

  case 6: /* command: LEX_LOG_LEVEL LEX_EQ LEX_INT  */
#line 97 "conf.y"
                                      { (yyval.tuple) = tuple_i_new(LEX_LOG_LEVEL, (yyvsp[0].number)); }
#line 1588 "conf.c"
    break;

  case 7: /* command: LEX_RECONN_TIMER LEX_EQ LEX_INT  */
#line 98 "conf.y"
                                         { (yyval.tuple) = tuple_i_new(LEX_RECONN_TIMER, (yyvsp[0].number)); }
#line 1594 "conf.c"
    break;

  case 8: /* command: LEX_IP LEX_EQ LEX_STRING  */
#line 99 "conf.y"
                                  { (yyval.tuple) = tuple_s_new(LEX_IP, (yyvsp[0].string)); }
#line 1600 "conf.c"
    break;

  case 9: /* command: LEX_PORT LEX_EQ LEX_INT  */
#line 100 "conf.y"
                                 { (yyval.tuple) = tuple_i_new(LEX_PORT, (yyvsp[0].number)); }
#line 1606 "conf.c"
    break;

  case 10: /* command: LEX_CSS LEX_EQ LEX_BOOL  */
#line 101 "conf.y"
                                 { (yyval.tuple) = tuple_i_new(LEX_CSS, (yyvsp[0].number)); }
#line 1612 "conf.c"
    break;

  case 11: /* command: LEX_CSS_PEM LEX_EQ LEX_STRING  */
#line 102 "conf.y"
                                       { (yyval.tuple) = tuple_s_new(LEX_CSS_PEM, (yyvsp[0].string)); }
#line 1618 "conf.c"
    break;

  case 12: /* command: LEX_CSS_CIPHERS LEX_EQ LEX_STRING  */
#line 103 "conf.y"
                                           { (yyval.tuple) = tuple_s_new(LEX_CSS_CIPHERS, (yyvsp[0].string)); }
#line 1624 "conf.c"
    break;

  case 13: /* command: LEX_DEFAULT_CIPHERS LEX_EQ LEX_STRING  */
#line 104 "conf.y"
                                               { (yyval.tuple) = tuple_s_new(LEX_DEFAULT_CIPHERS, (yyvsp[0].string)); }
#line 1630 "conf.c"
    break;

  case 14: /* command: LEX_DH_PARAM LEX_EQ LEX_STRING  */
#line 105 "conf.y"
                                        { (yyval.tuple) = tuple_s_new(LEX_DH_PARAM, (yyvsp[0].string)); }
#line 1636 "conf.c"
    break;

  case 15: /* command: LEX_LOG LEX_EQ LEX_BOOL  */
#line 106 "conf.y"
                                 { (yyval.tuple) = tuple_i_new(LEX_LOG, (yyvsp[0].number)); }
#line 1642 "conf.c"
    break;

  case 16: /* command: LEX_LOG_SYSTEM LEX_EQ LEX_BOOL  */
#line 107 "conf.y"
                                        { (yyval.tuple) = tuple_i_new(LEX_LOG_SYSTEM, (yyvsp[0].number)); }
#line 1648 "conf.c"
    break;

  case 17: /* command: LEX_LOG_SYNC_INTERVAL LEX_EQ LEX_INT  */
#line 108 "conf.y"
                                              { (yyval.tuple) = tuple_i_new(
						LEX_LOG_SYNC_INTERVAL, (yyvsp[0].number)); }
#line 1655 "conf.c"
    break;

  case 18: /* command: LEX_PID_FILE LEX_EQ LEX_STRING  */
#line 110 "conf.y"
                                        { (yyval.tuple) = tuple_s_new(LEX_PID_FILE, (yyvsp[0].string)); }
#line 1661 "conf.c"
    break;

  case 19: /* command: LEX_WRITE_OIDENTD LEX_EQ LEX_BOOL  */
#line 111 "conf.y"
                                           { (yyval.tuple) = tuple_i_new(LEX_WRITE_OIDENTD, (yyvsp[0].number)); }
#line 1667 "conf.c"
    break;

  case 20: /* command: LEX_OIDENTD_FILE LEX_EQ LEX_STRING  */
#line 112 "conf.y"
                                            { (yyval.tuple) = tuple_s_new(LEX_OIDENTD_FILE, (yyvsp[0].string)); }
#line 1673 "conf.c"
    break;

  case 21: /* command: LEX_BACKLOG_LINES LEX_EQ LEX_INT  */
#line 114 "conf.y"
                                              {
	           (yyval.tuple) = tuple_i_new(LEX_BACKLOG_LINES, (yyvsp[0].number));
		  }
#line 1681 "conf.c"
    break;

  case 22: /* command: LEX_BACKLOG_TIMESTAMP LEX_EQ LEX_STRING  */
#line 117 "conf.y"
                                                     {
	       (yyval.tuple) = tuple_s_new(LEX_BACKLOG_TIMESTAMP, (yyvsp[0].string));
	       }
#line 1689 "conf.c"
    break;

  case 23: /* command: LEX_BACKLOG_NO_TIMESTAMP LEX_EQ LEX_BOOL  */
#line 120 "conf.y"
                                                      {
	       (yyval.tuple) = tuple_i_new(LEX_BACKLOG_NO_TIMESTAMP, (yyvsp[0].number));
	       }
#line 1697 "conf.c"
    break;

  case 24: /* command: LEX_BACKLOG LEX_EQ LEX_BOOL  */
#line 123 "conf.y"
                                         { (yyval.tuple) = tuple_i_new(LEX_BACKLOG, (yyvsp[0].number)); }
#line 1703 "conf.c"
    break;

  case 25: /* command: LEX_BLRESET_ON_TALK LEX_EQ LEX_BOOL  */
#line 124 "conf.y"
                                                 {
	       (yyval.tuple) = tuple_i_new(LEX_BLRESET_ON_TALK, (yyvsp[0].number));
	       }
#line 1711 "conf.c"
    break;

  case 26: /* command: LEX_BL_MSG_ONLY LEX_EQ LEX_BOOL  */
#line 127 "conf.y"
                                             {
	       (yyval.tuple) = tuple_i_new(LEX_BL_MSG_ONLY, (yyvsp[0].number));
	       }
#line 1719 "conf.c"
    break;

  case 27: /* command: LEX_ALWAYS_BACKLOG LEX_EQ LEX_BOOL  */
#line 130 "conf.y"
                                                { (yyval.tuple) = tuple_i_new(
						LEX_ALWAYS_BACKLOG, (yyvsp[0].number)); }
#line 1726 "conf.c"
    break;

  case 28: /* command: LEX_NETWORK LEX_LBRA network LEX_RBRA  */
#line 133 "conf.y"
                                               { (yyval.tuple) = tuple_l_new(LEX_NETWORK,
						(yyvsp[-1].list)); }
#line 1733 "conf.c"
    break;

  case 29: /* command: LEX_USER LEX_LBRA user LEX_RBRA  */
#line 135 "conf.y"
                                         { (yyval.tuple) = tuple_l_new(LEX_USER, (yyvsp[-1].list)); }
#line 1739 "conf.c"
    break;

  case 30: /* network: %empty  */
#line 138 "conf.y"
        { (yyval.list) = list_new(NULL); }
#line 1745 "conf.c"
    break;

  case 31: /* network: network net_command LEX_SEMICOLON  */
#line 139 "conf.y"
                                            { list_add_last((yyvsp[-2].list), (yyvsp[-1].tuple)); (yyval.list) = (yyvsp[-2].list); }
#line 1751 "conf.c"
    break;

  case 32: /* net_command: LEX_NAME LEX_EQ LEX_STRING  */
#line 142 "conf.y"
                                      { (yyval.tuple) = tuple_s_new(LEX_NAME, (yyvsp[0].string)); }
#line 1757 "conf.c"
    break;

  case 33: /* net_command: LEX_SSL LEX_EQ LEX_BOOL  */
#line 143 "conf.y"
                                     { (yyval.tuple) = tuple_i_new(LEX_SSL, (yyvsp[0].number)); }
#line 1763 "conf.c"
    break;

  case 34: /* net_command: LEX_CIPHERS LEX_EQ LEX_STRING  */
#line 144 "conf.y"
                                           { (yyval.tuple) = tuple_s_new(LEX_CIPHERS, (yyvsp[0].string)); }
#line 1769 "conf.c"
    break;

  case 35: /* net_command: LEX_SERVER LEX_LBRA server LEX_RBRA  */
#line 145 "conf.y"
                                                 {
			(yyval.tuple) = tuple_l_new(LEX_SERVER, (yyvsp[-1].list)); }
#line 1776 "conf.c"
    break;

  case 36: /* user: %empty  */
#line 149 "conf.y"
    { (yyval.list) = list_new(NULL); }
#line 1782 "conf.c"
    break;

  case 37: /* user: user usr_command LEX_SEMICOLON  */
#line 150 "conf.y"
                                     { list_add_last((yyvsp[-2].list), (yyvsp[-1].tuple)); (yyval.list) = (yyvsp[-2].list); }
#line 1788 "conf.c"
    break;

  case 38: /* usr_command: LEX_NAME LEX_EQ LEX_STRING  */
#line 153 "conf.y"
                                      {
		   (yyval.tuple) = tuple_s_new(LEX_NAME, (yyvsp[0].string)); }
#line 1795 "conf.c"
    break;

  case 39: /* usr_command: LEX_PASSWORD LEX_EQ LEX_STRING  */
#line 155 "conf.y"
                                            { (yyval.tuple) = tuple_s_new(LEX_PASSWORD,
		(yyvsp[0].string)); }
#line 1802 "conf.c"
    break;

  case 40: /* usr_command: LEX_ADMIN LEX_EQ LEX_BOOL  */
#line 157 "conf.y"
                                       { (yyval.tuple) = tuple_i_new(LEX_ADMIN, (yyvsp[0].number)); }
#line 1808 "conf.c"
    break;

  case 41: /* usr_command: LEX_BIP_USE_NOTICE LEX_EQ LEX_BOOL  */
#line 158 "conf.y"
                                                {
		   (yyval.tuple) = tuple_i_new(LEX_BIP_USE_NOTICE, (yyvsp[0].number)); }
#line 1815 "conf.c"
    break;

  case 42: /* usr_command: LEX_SSL_CHECK_MODE LEX_EQ LEX_STRING  */
#line 160 "conf.y"
                                                  { (yyval.tuple) = tuple_s_new(
			LEX_SSL_CHECK_MODE, (yyvsp[0].string)); }
#line 1822 "conf.c"
    break;

  case 43: /* usr_command: LEX_SSL_CHECK_STORE LEX_EQ LEX_STRING  */
#line 162 "conf.y"
                                                   { (yyval.tuple) = tuple_s_new(
			LEX_SSL_CHECK_STORE, (yyvsp[0].string)); }
#line 1829 "conf.c"
    break;

  case 44: /* usr_command: LEX_SSL_CLIENT_CERTFILE LEX_EQ LEX_STRING  */
#line 164 "conf.y"
                                                       { (yyval.tuple) = tuple_s_new(
			LEX_SSL_CLIENT_CERTFILE, (yyvsp[0].string)); }
#line 1836 "conf.c"
    break;

  case 45: /* usr_command: LEX_DEFAULT_USER LEX_EQ LEX_STRING  */
#line 166 "conf.y"
                                                {
		   (yyval.tuple) = tuple_s_new(LEX_DEFAULT_USER, (yyvsp[0].string)); }
#line 1843 "conf.c"
    break;

  case 46: /* usr_command: LEX_DEFAULT_NICK LEX_EQ LEX_STRING  */
#line 168 "conf.y"
                                                {
		   (yyval.tuple) = tuple_s_new(LEX_DEFAULT_NICK, (yyvsp[0].string)); }
#line 1850 "conf.c"
    break;

  case 47: /* usr_command: LEX_DEFAULT_REALNAME LEX_EQ LEX_STRING  */
#line 170 "conf.y"
                                                    {
		   (yyval.tuple) = tuple_s_new(LEX_DEFAULT_REALNAME, (yyvsp[0].string)); }
#line 1857 "conf.c"
    break;

  case 48: /* usr_command: LEX_BACKLOG_LINES LEX_EQ LEX_INT  */
#line 172 "conf.y"
                                              {
	           (yyval.tuple) = tuple_i_new(LEX_BACKLOG_LINES, (yyvsp[0].number));
		  }
#line 1865 "conf.c"
    break;

  case 49: /* usr_command: LEX_BACKLOG_TIMESTAMP LEX_EQ LEX_STRING  */
#line 175 "conf.y"
                                                     {
	       (yyval.tuple) = tuple_s_new(LEX_BACKLOG_TIMESTAMP, (yyvsp[0].string));
	       }
#line 1873 "conf.c"
    break;

  case 50: /* usr_command: LEX_BACKLOG_NO_TIMESTAMP LEX_EQ LEX_BOOL  */
#line 178 "conf.y"
                                                      {
	       (yyval.tuple) = tuple_i_new(LEX_BACKLOG_NO_TIMESTAMP, (yyvsp[0].number));
	       }
#line 1881 "conf.c"
    break;

  case 51: /* usr_command: LEX_BACKLOG LEX_EQ LEX_BOOL  */
#line 181 "conf.y"
                                         { (yyval.tuple) = tuple_i_new(LEX_BACKLOG, (yyvsp[0].number)); }
#line 1887 "conf.c"
    break;

  case 52: /* usr_command: LEX_BLRESET_ON_TALK LEX_EQ LEX_BOOL  */
#line 182 "conf.y"
                                                 {
	       (yyval.tuple) = tuple_i_new(LEX_BLRESET_ON_TALK, (yyvsp[0].number));
	       }
#line 1895 "conf.c"
    break;

  case 53: /* usr_command: LEX_BLRESET_CONNECTION LEX_EQ LEX_BOOL  */
#line 185 "conf.y"
                                                    {
	       (yyval.tuple) = tuple_i_new(LEX_BLRESET_CONNECTION, (yyvsp[0].number));
	       }
#line 1903 "conf.c"
    break;

  case 54: /* usr_command: LEX_BL_MSG_ONLY LEX_EQ LEX_BOOL  */
#line 188 "conf.y"
                                             {
	       (yyval.tuple) = tuple_i_new(LEX_BL_MSG_ONLY, (yyvsp[0].number));
	       }
#line 1911 "conf.c"
    break;

  case 55: /* usr_command: LEX_ALWAYS_BACKLOG LEX_EQ LEX_BOOL  */
#line 191 "conf.y"
                                                { (yyval.tuple) = tuple_i_new(
						LEX_ALWAYS_BACKLOG, (yyvsp[0].number)); }
#line 1918 "conf.c"
    break;

  case 56: /* usr_command: LEX_CONNECTION LEX_LBRA connection LEX_RBRA  */
#line 193 "conf.y"
                                                         {
				 (yyval.tuple) = tuple_l_new(LEX_CONNECTION, (yyvsp[-1].list)); }
#line 1925 "conf.c"
    break;

  case 57: /* connection: %empty  */
#line 197 "conf.y"
          { (yyval.list) = list_new(NULL); }
#line 1931 "conf.c"
    break;

  case 58: /* connection: connection con_command LEX_SEMICOLON  */
#line 198 "conf.y"
                                                 {
	       list_add_last((yyvsp[-2].list), (yyvsp[-1].tuple)); (yyval.list) = (yyvsp[-2].list); }
#line 1938 "conf.c"
    break;

  case 59: /* con_command: LEX_NAME LEX_EQ LEX_STRING  */
#line 202 "conf.y"
                                      { (yyval.tuple) = tuple_s_new(LEX_NAME, (yyvsp[0].string)); }
#line 1944 "conf.c"
    break;

  case 60: /* con_command: LEX_NETWORK LEX_EQ LEX_STRING  */
#line 203 "conf.y"
                                           { (yyval.tuple) = tuple_s_new(LEX_NETWORK,
			 (yyvsp[0].string)); }
#line 1951 "conf.c"
    break;

  case 61: /* con_command: LEX_LOG LEX_EQ LEX_BOOL  */
#line 205 "conf.y"
                                     { (yyval.tuple) = tuple_i_new(LEX_LOG, (yyvsp[0].number)); }
#line 1957 "conf.c"
    break;

  case 62: /* con_command: LEX_NICK LEX_EQ LEX_STRING  */
#line 206 "conf.y"
                                        { (yyval.tuple) = tuple_s_new(LEX_NICK, (yyvsp[0].string)); }
#line 1963 "conf.c"
    break;

  case 63: /* con_command: LEX_USER LEX_EQ LEX_STRING  */
#line 207 "conf.y"
                                        { (yyval.tuple) = tuple_s_new(LEX_USER, (yyvsp[0].string)); }
#line 1969 "conf.c"
    break;

  case 64: /* con_command: LEX_REALNAME LEX_EQ LEX_STRING  */
#line 208 "conf.y"
                                            { (yyval.tuple) = tuple_s_new(LEX_REALNAME,
		 (yyvsp[0].string)); }
#line 1976 "conf.c"
    break;

  case 65: /* con_command: LEX_PASSWORD LEX_EQ LEX_STRING  */
#line 210 "conf.y"
                                            { (yyval.tuple) = tuple_s_new(LEX_PASSWORD,
		 (yyvsp[0].string)); }
#line 1983 "conf.c"
    break;

  case 66: /* con_command: LEX_SASL_USERNAME LEX_EQ LEX_STRING  */
#line 212 "conf.y"
                                                 { (yyval.tuple) = tuple_s_new(
	     LEX_SASL_USERNAME, (yyvsp[0].string)); }
#line 1990 "conf.c"
    break;

  case 67: /* con_command: LEX_SASL_PASSWORD LEX_EQ LEX_STRING  */
#line 214 "conf.y"
                                                 { (yyval.tuple) = tuple_s_new(
	     LEX_SASL_PASSWORD, (yyvsp[0].string)); }
#line 1997 "conf.c"
    break;

  case 68: /* con_command: LEX_SASL_MECHANISM LEX_EQ LEX_STRING  */
#line 216 "conf.y"
                                                  { (yyval.tuple) = tuple_s_new(
	     LEX_SASL_MECHANISM, (yyvsp[0].string)); }
#line 2004 "conf.c"
    break;

  case 69: /* con_command: LEX_VHOST LEX_EQ LEX_STRING  */
#line 218 "conf.y"
                                         { (yyval.tuple) = tuple_s_new(LEX_VHOST, (yyvsp[0].string)); }
#line 2010 "conf.c"
    break;

  case 70: /* con_command: LEX_SOURCE_PORT LEX_EQ LEX_INT  */
#line 219 "conf.y"
                                            {
		   (yyval.tuple) = tuple_i_new(LEX_SOURCE_PORT, (yyvsp[0].number)); }
#line 2017 "conf.c"
    break;

  case 71: /* con_command: LEX_AWAY_NICK LEX_EQ LEX_STRING  */
#line 221 "conf.y"
                                             { (yyval.tuple) = tuple_s_new(LEX_AWAY_NICK,
						(yyvsp[0].string)); }
#line 2024 "conf.c"
    break;

  case 72: /* con_command: LEX_FOLLOW_NICK LEX_EQ LEX_BOOL  */
#line 223 "conf.y"
                                             {
		   (yyval.tuple) = tuple_i_new(LEX_FOLLOW_NICK, (yyvsp[0].number)); }
#line 2031 "conf.c"
    break;

  case 73: /* con_command: LEX_IGN_FIRST_NICK LEX_EQ LEX_BOOL  */
#line 225 "conf.y"
                                                { (yyval.tuple) = tuple_i_new(
					   LEX_IGN_FIRST_NICK, (yyvsp[0].number)); }
#line 2038 "conf.c"
    break;

  case 74: /* con_command: LEX_AUTOJOIN_ON_KICK LEX_EQ LEX_BOOL  */
#line 227 "conf.y"
                                                  {
		   (yyval.tuple) = tuple_i_new(LEX_AUTOJOIN_ON_KICK, (yyvsp[0].number)); }
#line 2045 "conf.c"
    break;

  case 75: /* con_command: LEX_IGNORE_CAPAB LEX_EQ LEX_BOOL  */
#line 229 "conf.y"
                                              {
		   (yyval.tuple) = tuple_i_new(LEX_IGNORE_CAPAB, (yyvsp[0].number)); }
#line 2052 "conf.c"
    break;

  case 76: /* con_command: LEX_CHANNEL LEX_LBRA channel LEX_RBRA  */
#line 231 "conf.y"
                                                   { (yyval.tuple) = tuple_l_new(
						LEX_CHANNEL, (yyvsp[-1].list)); }
#line 2059 "conf.c"
    break;

  case 77: /* con_command: LEX_ON_CONNECT_SEND LEX_EQ LEX_STRING  */
#line 233 "conf.y"
                                                   { (yyval.tuple) = tuple_s_new(
						LEX_ON_CONNECT_SEND, (yyvsp[0].string)); }
#line 2066 "conf.c"
    break;

  case 78: /* con_command: LEX_NO_CLIENT_AWAY_MSG LEX_EQ LEX_STRING  */
#line 235 "conf.y"
                                                      { (yyval.tuple) = tuple_s_new(
						LEX_NO_CLIENT_AWAY_MSG, (yyvsp[0].string)); }
#line 2073 "conf.c"
    break;

  case 79: /* con_command: LEX_SSL_CHECK_MODE LEX_EQ LEX_STRING  */
#line 237 "conf.y"
                                                  { (yyval.tuple) = tuple_s_new(
			LEX_SSL_CHECK_MODE, (yyvsp[0].string)); }
#line 2080 "conf.c"
    break;

  case 80: /* channel: %empty  */
#line 240 "conf.y"
       { (yyval.list) = list_new(NULL); }
#line 2086 "conf.c"
    break;

  case 81: /* channel: channel cha_command LEX_SEMICOLON  */
#line 241 "conf.y"
                                           { list_add_last((yyvsp[-2].list), (yyvsp[-1].tuple)); (yyval.list) = (yyvsp[-2].list); }
#line 2092 "conf.c"
    break;

  case 82: /* cha_command: LEX_NAME LEX_EQ LEX_STRING  */
#line 244 "conf.y"
                                      { (yyval.tuple) = tuple_s_new(LEX_NAME, (yyvsp[0].string)); }
#line 2098 "conf.c"
    break;

  case 83: /* cha_command: LEX_KEY LEX_EQ LEX_STRING  */
#line 245 "conf.y"
                                       { (yyval.tuple) = tuple_s_new(LEX_KEY, (yyvsp[0].string)); }
#line 2104 "conf.c"
    break;

  case 84: /* cha_command: LEX_BACKLOG LEX_EQ LEX_BOOL  */
#line 246 "conf.y"
                                         { (yyval.tuple) = tuple_i_new(LEX_BACKLOG, (yyvsp[0].number)); }
#line 2110 "conf.c"
    break;

  case 85: /* server: %empty  */
#line 249 "conf.y"
       { (yyval.list) = list_new(NULL); }
#line 2116 "conf.c"
    break;

  case 86: /* server: server ser_command LEX_SEMICOLON  */
#line 250 "conf.y"
                                          { list_add_last((yyvsp[-2].list), (yyvsp[-1].tuple)); (yyval.list) = (yyvsp[-2].list); }
#line 2122 "conf.c"
    break;

  case 87: /* ser_command: LEX_HOST LEX_EQ LEX_STRING  */
#line 253 "conf.y"
                                       { (yyval.tuple) = tuple_s_new(LEX_HOST, (yyvsp[0].string)); }
#line 2128 "conf.c"
    break;

  case 88: /* ser_command: LEX_PORT LEX_EQ LEX_INT  */
#line 254 "conf.y"
                                     { (yyval.tuple) = tuple_i_new(LEX_PORT, (yyvsp[0].number)); }
#line 2134 "conf.c"
    break;


#line 2138 "conf.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

