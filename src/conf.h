/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_CONF_H_INCLUDED
# define YY_YY_CONF_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    LEX_IP = 258,                  /* LEX_IP  */
    LEX_EQ = 259,                  /* LEX_EQ  */
    LEX_PORT = 260,                /* LEX_PORT  */
    LEX_CSS = 261,                 /* LEX_CSS  */
    LEX_SEMICOLON = 262,           /* LEX_SEMICOLON  */
    LEX_CONNECTION = 263,          /* LEX_CONNECTION  */
    LEX_NETWORK = 264,             /* LEX_NETWORK  */
    LEX_LBRA = 265,                /* LEX_LBRA  */
    LEX_RBRA = 266,                /* LEX_RBRA  */
    LEX_USER = 267,                /* LEX_USER  */
    LEX_NAME = 268,                /* LEX_NAME  */
    LEX_NICK = 269,                /* LEX_NICK  */
    LEX_SERVER = 270,              /* LEX_SERVER  */
    LEX_PASSWORD = 271,            /* LEX_PASSWORD  */
    LEX_SRCIP = 272,               /* LEX_SRCIP  */
    LEX_HOST = 273,                /* LEX_HOST  */
    LEX_VHOST = 274,               /* LEX_VHOST  */
    LEX_SOURCE_PORT = 275,         /* LEX_SOURCE_PORT  */
    LEX_NONE = 276,                /* LEX_NONE  */
    LEX_COMMENT = 277,             /* LEX_COMMENT  */
    LEX_BUNCH = 278,               /* LEX_BUNCH  */
    LEX_REALNAME = 279,            /* LEX_REALNAME  */
    LEX_SSL = 280,                 /* LEX_SSL  */
    LEX_SSL_CHECK_MODE = 281,      /* LEX_SSL_CHECK_MODE  */
    LEX_SSL_CHECK_STORE = 282,     /* LEX_SSL_CHECK_STORE  */
    LEX_SSL_CLIENT_CERTFILE = 283, /* LEX_SSL_CLIENT_CERTFILE  */
    LEX_CIPHERS = 284,             /* LEX_CIPHERS  */
    LEX_CSS_CIPHERS = 285,         /* LEX_CSS_CIPHERS  */
    LEX_DEFAULT_CIPHERS = 286,     /* LEX_DEFAULT_CIPHERS  */
    LEX_DH_PARAM = 287,            /* LEX_DH_PARAM  */
    LEX_CHANNEL = 288,             /* LEX_CHANNEL  */
    LEX_KEY = 289,                 /* LEX_KEY  */
    LEX_LOG_ROOT = 290,            /* LEX_LOG_ROOT  */
    LEX_LOG_FORMAT = 291,          /* LEX_LOG_FORMAT  */
    LEX_LOG_LEVEL = 292,           /* LEX_LOG_LEVEL  */
    LEX_BACKLOG_LINES = 293,       /* LEX_BACKLOG_LINES  */
    LEX_BACKLOG_TIMESTAMP = 294,   /* LEX_BACKLOG_TIMESTAMP  */
    LEX_BACKLOG_NO_TIMESTAMP = 295, /* LEX_BACKLOG_NO_TIMESTAMP  */
    LEX_BACKLOG = 296,             /* LEX_BACKLOG  */
    LEX_LOG = 297,                 /* LEX_LOG  */
    LEX_LOG_SYSTEM = 298,          /* LEX_LOG_SYSTEM  */
    LEX_LOG_SYNC_INTERVAL = 299,   /* LEX_LOG_SYNC_INTERVAL  */
    LEX_FOLLOW_NICK = 300,         /* LEX_FOLLOW_NICK  */
    LEX_ON_CONNECT_SEND = 301,     /* LEX_ON_CONNECT_SEND  */
    LEX_AWAY_NICK = 302,           /* LEX_AWAY_NICK  */
    LEX_PID_FILE = 303,            /* LEX_PID_FILE  */
    LEX_WRITE_OIDENTD = 304,       /* LEX_WRITE_OIDENTD  */
    LEX_OIDENTD_FILE = 305,        /* LEX_OIDENTD_FILE  */
    LEX_IGN_FIRST_NICK = 306,      /* LEX_IGN_FIRST_NICK  */
    LEX_ALWAYS_BACKLOG = 307,      /* LEX_ALWAYS_BACKLOG  */
    LEX_BLRESET_ON_TALK = 308,     /* LEX_BLRESET_ON_TALK  */
    LEX_BLRESET_CONNECTION = 309,  /* LEX_BLRESET_CONNECTION  */
    LEX_DEFAULT_USER = 310,        /* LEX_DEFAULT_USER  */
    LEX_DEFAULT_NICK = 311,        /* LEX_DEFAULT_NICK  */
    LEX_DEFAULT_REALNAME = 312,    /* LEX_DEFAULT_REALNAME  */
    LEX_NO_CLIENT_AWAY_MSG = 313,  /* LEX_NO_CLIENT_AWAY_MSG  */
    LEX_BL_MSG_ONLY = 314,         /* LEX_BL_MSG_ONLY  */
    LEX_ADMIN = 315,               /* LEX_ADMIN  */
    LEX_BIP_USE_NOTICE = 316,      /* LEX_BIP_USE_NOTICE  */
    LEX_CSS_PEM = 317,             /* LEX_CSS_PEM  */
    LEX_AUTOJOIN_ON_KICK = 318,    /* LEX_AUTOJOIN_ON_KICK  */
    LEX_IGNORE_CAPAB = 319,        /* LEX_IGNORE_CAPAB  */
    LEX_RECONN_TIMER = 320,        /* LEX_RECONN_TIMER  */
    LEX_SASL_USERNAME = 321,       /* LEX_SASL_USERNAME  */
    LEX_SASL_PASSWORD = 322,       /* LEX_SASL_PASSWORD  */
    LEX_SASL_MECHANISM = 323,      /* LEX_SASL_MECHANISM  */
    LEX_BOOL = 324,                /* LEX_BOOL  */
    LEX_INT = 325,                 /* LEX_INT  */
    LEX_STRING = 326               /* LEX_STRING  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define LEX_IP 258
#define LEX_EQ 259
#define LEX_PORT 260
#define LEX_CSS 261
#define LEX_SEMICOLON 262
#define LEX_CONNECTION 263
#define LEX_NETWORK 264
#define LEX_LBRA 265
#define LEX_RBRA 266
#define LEX_USER 267
#define LEX_NAME 268
#define LEX_NICK 269
#define LEX_SERVER 270
#define LEX_PASSWORD 271
#define LEX_SRCIP 272
#define LEX_HOST 273
#define LEX_VHOST 274
#define LEX_SOURCE_PORT 275
#define LEX_NONE 276
#define LEX_COMMENT 277
#define LEX_BUNCH 278
#define LEX_REALNAME 279
#define LEX_SSL 280
#define LEX_SSL_CHECK_MODE 281
#define LEX_SSL_CHECK_STORE 282
#define LEX_SSL_CLIENT_CERTFILE 283
#define LEX_CIPHERS 284
#define LEX_CSS_CIPHERS 285
#define LEX_DEFAULT_CIPHERS 286
#define LEX_DH_PARAM 287
#define LEX_CHANNEL 288
#define LEX_KEY 289
#define LEX_LOG_ROOT 290
#define LEX_LOG_FORMAT 291
#define LEX_LOG_LEVEL 292
#define LEX_BACKLOG_LINES 293
#define LEX_BACKLOG_TIMESTAMP 294
#define LEX_BACKLOG_NO_TIMESTAMP 295
#define LEX_BACKLOG 296
#define LEX_LOG 297
#define LEX_LOG_SYSTEM 298
#define LEX_LOG_SYNC_INTERVAL 299
#define LEX_FOLLOW_NICK 300
#define LEX_ON_CONNECT_SEND 301
#define LEX_AWAY_NICK 302
#define LEX_PID_FILE 303
#define LEX_WRITE_OIDENTD 304
#define LEX_OIDENTD_FILE 305
#define LEX_IGN_FIRST_NICK 306
#define LEX_ALWAYS_BACKLOG 307
#define LEX_BLRESET_ON_TALK 308
#define LEX_BLRESET_CONNECTION 309
#define LEX_DEFAULT_USER 310
#define LEX_DEFAULT_NICK 311
#define LEX_DEFAULT_REALNAME 312
#define LEX_NO_CLIENT_AWAY_MSG 313
#define LEX_BL_MSG_ONLY 314
#define LEX_ADMIN 315
#define LEX_BIP_USE_NOTICE 316
#define LEX_CSS_PEM 317
#define LEX_AUTOJOIN_ON_KICK 318
#define LEX_IGNORE_CAPAB 319
#define LEX_RECONN_TIMER 320
#define LEX_SASL_USERNAME 321
#define LEX_SASL_PASSWORD 322
#define LEX_SASL_MECHANISM 323
#define LEX_BOOL 324
#define LEX_INT 325
#define LEX_STRING 326

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 74 "conf.y"

	int number;
	char *string;
	void *list;
	struct tuple *tuple;

#line 216 "conf.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_CONF_H_INCLUDED  */
